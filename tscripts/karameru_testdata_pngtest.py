# -*- coding: utf-8 -*-

from taf import *
from taf.tools import copy

module.TYPE = module.test

module.BUILDER = copy.files

module.TARGET = 'pngtest'

module.SOURCE = {
    'testdata' : {
        'pngtest' : [
            'pngtest.png',
            'pngtest_withalpha.png',
            'pngtest_16bit.png',
            'pngtest_grayscale.png',
            'pngtest_grayscale1bit.png',
            'pngtest_grayscale4bit.png',
            'pngtest_grayscalewithalpha.png',
            'pngtest_palette.png',
            'pngtest_palettewithalpha.png',
            'pngtest_failed.png',
        ]
    }
}
