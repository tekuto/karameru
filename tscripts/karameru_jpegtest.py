# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'karameru_testdata_jpegtest',
    'karameru_test',
    'karameru',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'karameru-jpegtest'

module.SOURCE = [
    'jpegtest.cpp',
]

module.LIB = [
    'jpeg',
]

module.USE = [
    'karameru-test',
    'karameru',
]
