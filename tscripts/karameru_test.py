# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.shlib

module.TARGET = 'karameru-test'

module.SOURCE = {
    'test' : [
        'picture.cpp',
    ]
}
