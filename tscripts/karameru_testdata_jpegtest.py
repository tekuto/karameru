# -*- coding: utf-8 -*-

from taf import *
from taf.tools import copy

module.TYPE = module.test

module.BUILDER = copy.files

module.TARGET = 'jpegtest'

module.SOURCE = {
    'testdata' : {
        'jpegtest' : [
            'jpegtest.jpg',
            'jpegtest_grayscale.jpg',
            'jpegtest_failed.jpg',
            'jpegtest_failed.png',
        ]
    }
}
