# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'karameru_files',
    'karameru_jpegtest',
    'karameru_pngtest',
]

module.BUILDER = cpp.shlib

module.TARGET = 'karameru'

module.SOURCE = [
    'jpeg.cpp',
    'png.cpp',
]

module.LIB = [
    'jpeg',
    'png',
]
