# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'karameru_testdata_pngtest',
    'karameru_test',
    'karameru',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'karameru-pngtest'

module.SOURCE = [
    'pngtest.cpp',
]

module.LIB = [
    'png',
]

module.USE = [
    'karameru-test',
    'karameru',
]
