﻿#include "fg/util/export.h"
#include "karameru/jpeg.h"
#include "fg/gl/picture.h"
#include "fg/gl/gl.h"

#include <memory>
#include <vector>
#include <csetjmp>
#include <jpeglib.h>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    struct MyErrorManager
    {
        jpeg_error_mgr  jpegErrorManager;
        std::jmp_buf    jumpBuffer;
    };

    struct DestroyDecompresser
    {
        void operator()(
            jpeg_decompress_struct *    _decompresserPtr
        ) const
        {
            jpeg_destroy_decompress( _decompresserPtr );
        }
    };

    using DecompresserDestroyer = std::unique_ptr<
        jpeg_decompress_struct
        , DestroyDecompresser
    >;

    struct FinishDecompress
    {
        void operator()(
            jpeg_decompress_struct *    _decompresserPtr
        ) const
        {
            jpeg_finish_decompress( _decompresserPtr );
        }
    };

    using DecompressFinisher = std::unique_ptr<
        jpeg_decompress_struct
        , FinishDecompress
    >;

    using Buffer = std::vector< unsigned char >;

    void errorExit(
        j_common_ptr    _commonPtr
    )
    {
        auto &  jpegErrorManager = *( _commonPtr->err );
        auto &  errorManager = reinterpret_cast< MyErrorManager & >( jpegErrorManager );

        jpegErrorManager.output_message( _commonPtr );

        std::longjmp(
            errorManager.jumpBuffer
            , 1
        );
    }

    bool initFormat(
        fg::GLenum &                                                _format
        , decltype( jpeg_decompress_struct::out_color_components )  _colorComponents
    )
    {
        auto    format = fg::GLenum();

        if( _colorComponents == 1 ) {
            format = fg::GL_LUMINANCE;
        } else if( _colorComponents == 3 ) {
            format = fg::GL_RGB;
        } else {
#ifdef  DEBUG
            std::printf(
                "E:非対応のフォーマット:%d\n"
                , _colorComponents
            );
#endif  // DEBUG

            return false;
        }

        _format = format;

        return true;
    }

    bool decompress(
        fg::GLsizei &               _width
        , fg::GLsizei &             _height
        , fg::GLenum &              _format
        , Buffer &                  _buffer
        , jpeg_decompress_struct &  _decompresser
        , const void *              _DATA
        , std::size_t               _size
    )
    {
        jpeg_mem_src(
            &_decompresser
            , static_cast< const unsigned char * >( _DATA )
            , _size
        );

        jpeg_read_header(
            &_decompresser
            , TRUE
        );

        jpeg_start_decompress( &_decompresser );
        auto    decompressFinisher = DecompressFinisher( &_decompresser );

        const auto &    WIDTH = _decompresser.output_width;
        const auto &    HEIGHT = _decompresser.output_height;

        auto    format = fg::GLenum();
        if( initFormat(
            format
            , _decompresser.out_color_components
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:フォーマットの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        const auto  ROW_STRIDE = WIDTH * _decompresser.output_components;
        const auto  SIZE = ROW_STRIDE * HEIGHT;

        auto    buffer = Buffer( SIZE );

        auto    bufferPtr = buffer.data() + SIZE - ROW_STRIDE;
        while( _decompresser.output_scanline < HEIGHT ) {
            jpeg_read_scanlines(
                &_decompresser
                , &bufferPtr
                , 1
            );

            bufferPtr -= ROW_STRIDE;
        }

        if( _decompresser.err->num_warnings > 0 ) {
#ifdef  DEBUG
            std::printf( "E:JPEG復号中に警告が発生\n" );
#endif  // DEBUG

            return false;
        }

        _width = WIDTH;
        _height = HEIGHT;
        _format = format;
        _buffer = std::move( buffer );

        return true;
    }
}

namespace karameru {
    FgGLPicture * generateGLPictureFromJpegImpl(
        const void *    _DATA
        , std::size_t   _size
    )
    {
        auto    decompresser = jpeg_decompress_struct();
        auto    errorManager = MyErrorManager();
        auto &  jpegErrorManager = errorManager.jpegErrorManager;

        decompresser.err = jpeg_std_error( &jpegErrorManager );
        jpegErrorManager.error_exit = errorExit;

        auto    decompresserDestroyer = DecompresserDestroyer();    // std::longjmp()後にjpeg_destroy_decompress()を呼び出すため、setjmp()より前に配置

        if( setjmp( errorManager.jumpBuffer ) != 0 ) {
#ifdef  DEBUG
            std::printf( "E:JPEG復号中にエラーが発生\n" );
#endif  // DEBUG

            return nullptr;
        }

        jpeg_create_decompress( &decompresser );
        decompresserDestroyer.reset( &decompresser );

        auto    width = fg::GLsizei();
        auto    height = fg::GLsizei();
        auto    format = fg::GLenum();
        auto    buffer = Buffer();

        if( decompress(
            width
            , height
            , format
            , buffer
            , decompresser
            , _DATA
            , _size
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:JPEG復号化に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return fgGLPictureCreate(
            buffer.data()
            , buffer.size()
            , width
            , height
            , format
            , fg::GL_UNSIGNED_BYTE
        );
    }
}
