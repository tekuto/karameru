﻿#include "fg/util/export.h"
#include "karameru/png.h"
#include "fg/gl/picture.h"
#include "fg/gl/gl.h"
#include "fg/common/unique.h"

#include <memory>
#include <vector>
#include <cstddef>
#include <cstring>
#include <csetjmp>
#include <png.h>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    struct PngReadStructs
    {
        png_struct *    pngPtr = nullptr;
        png_info *      infoPtr = nullptr;
    };

    struct DestroyPngReadStructs
    {
        void operator()(
            PngReadStructs *    _pngReadStructsPtr
        ) const
        {
            auto &  infoPtr = _pngReadStructsPtr->infoPtr;

            png_destroy_read_struct(
                &( _pngReadStructsPtr->pngPtr )
                , infoPtr != nullptr ? &infoPtr : nullptr
                , nullptr
            );
        }
    };

    using PngReadStructsDestroyer = std::unique_ptr<
        PngReadStructs
        , DestroyPngReadStructs
    >;

    using Buffer = std::vector< png_byte >;

    using BufferPointers = std::vector< Buffer::value_type * >;

    struct ReadInfo
    {
        const void *        DATA;
        const std::size_t   SIZE;

        std::size_t position = 0;
    };

    void readPng(
        png_struct *    _pngPtr
        , png_byte *    _data
        , png_size_t    _length
    )
    {
        auto &  readInfo = *static_cast< ReadInfo * >( png_get_io_ptr( _pngPtr ) );

        const auto &    SIZE = readInfo.SIZE;

        auto &  position = readInfo.position;

        const auto  BEGIN = position;

        position += _length;
        if( position > SIZE ) {
            position = SIZE;
        }

        auto    readLength = position - BEGIN;

        std::memset(
            _data
            , 0x00
            , _length
        );

        std::memcpy(
            _data
            , static_cast< const char * >( readInfo.DATA ) + BEGIN
            , readLength
        );
    }

    bool initFormat(
        fg::GLenum &    _format
        , int           _colorType
        , bool          _existsTrnsChunk
    )
    {
        auto    format = fg::GLenum();

        if( _colorType == PNG_COLOR_TYPE_GRAY ) {
            format = fg::GL_LUMINANCE;
        } else if( _colorType == PNG_COLOR_TYPE_GRAY_ALPHA ) {
            format = fg::GL_LUMINANCE_ALPHA;
        } else if( _colorType == PNG_COLOR_TYPE_PALETTE ) {
            if( _existsTrnsChunk == true ) {
                format = fg::GL_RGBA;
            } else {
                format = fg::GL_RGB;
            }
        } else if( _colorType == PNG_COLOR_TYPE_RGB ) {
            format = fg::GL_RGB;
        } else if( _colorType == PNG_COLOR_TYPE_RGB_ALPHA ) {
            format = fg::GL_RGBA;
        } else {
#ifdef  DEBUG
            std::printf(
                "E:非対応のフォーマット:%d\n"
                , _colorType
            );
#endif  // DEBUG

            return false;
        }

        _format = format;

        return true;
    }

    bool initDataType(
        fg::GLenum &    _dataType
        , int &         _depthSize
        , int           _bitDepth
        , int           _colorType
    )
    {
        auto    dataType = fg::GLenum();
        auto    depthSize = int();

        if( _colorType == PNG_COLOR_TYPE_PALETTE ) {
            dataType = fg::GL_UNSIGNED_BYTE;
            depthSize = 1;
        } if( _bitDepth < 8 ) {
            dataType = fg::GL_UNSIGNED_BYTE;
            depthSize = 1;
        } else if( _bitDepth == 8 ) {
            dataType = fg::GL_UNSIGNED_BYTE;
            depthSize = 1;
        } else if( _bitDepth == 16 ) {
            dataType = fg::GL_UNSIGNED_SHORT;
            depthSize = 2;
        } else {
#ifdef  DEBUG
            std::printf(
                "E:非対応のデータタイプ:%d\n"
                , _bitDepth
            );
#endif  // DEBUG

            return false;
        }

        _dataType = dataType;
        _depthSize = depthSize;

        return true;
    }

    void initChannels(
        png_byte &      _channels
        , png_struct *  _pngPtr
        , png_info *    _infoPtr
        , int           _colorType
        , bool          _existsTrnsChunk
    )
    {
        auto    channels = png_byte();

        if( _colorType == PNG_COLOR_TYPE_PALETTE ) {
            if( _existsTrnsChunk == true ) {
                channels = 4;
            } else {
                channels = 3;
            }
        } else {
            channels = png_get_channels(
                _pngPtr
                , _infoPtr
            );
        }

        _channels = channels;
    }

    void transform(
        png_struct *    _pngPtr
        , int           _bitDepth
        , int           _colorType
    )
    {
        if( _colorType == PNG_COLOR_TYPE_GRAY && _bitDepth < 8 ) {
            png_set_expand_gray_1_2_4_to_8( _pngPtr );
        }
        if( _colorType == PNG_COLOR_TYPE_PALETTE ) {
            png_set_palette_to_rgb( _pngPtr );
        }
    }

    void initBufferPointers(
        BufferPointers &    _bufferPointers
        , Buffer &          _buffer
        , png_uint_32       _rowStride
        , png_uint_32       _size
    )
    {
        auto    bufferPtr = _buffer.data() + _size - _rowStride;
        for( auto & bufferPointer : _bufferPointers ) {
            bufferPointer = bufferPtr;

            bufferPtr -= _rowStride;
        }
    }

    bool decompress(
        fg::GLsizei &   _width
        , fg::GLsizei & _height
        , fg::GLenum &  _format
        , fg::GLenum &  _dataType
        , Buffer &      _buffer
        , png_struct *  _pngPtr
        , png_info *    _infoPtr
        , const void *  _DATA
        , std::size_t   _size
    )
    {
        auto    readInfo = ReadInfo{
            _DATA,
            _size,
        };

        png_set_read_fn(
            _pngPtr
            , &readInfo
            , readPng
        );

        png_read_info(
            _pngPtr
            , _infoPtr
        );

        auto    width = png_uint_32();
        auto    height = png_uint_32();
        auto    bitDepth = int();
        auto    colorType = int();

        png_get_IHDR(
            _pngPtr
            , _infoPtr
            , &width
            , &height
            , &bitDepth
            , &colorType
            , nullptr
            , nullptr
            , nullptr
        );

        const auto  EXISTS_TRNS_CHUNK = png_get_valid(
            _pngPtr
            , _infoPtr
            , PNG_INFO_tRNS
        ) != 0;

        auto    format = fg::GLenum();
        if( initFormat(
            format
            , colorType
            , EXISTS_TRNS_CHUNK
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:フォーマットの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        auto    dataType = fg::GLenum();
        auto    depthSize = int();
        if( initDataType(
            dataType
            , depthSize
            , bitDepth
            , colorType
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:データタイプの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        auto    channels = png_byte();
        initChannels(
            channels
            , _pngPtr
            , _infoPtr
            , colorType
            , EXISTS_TRNS_CHUNK
        );

        const auto  ROW_STRIDE = width * depthSize * channels;
        const auto  SIZE = ROW_STRIDE * height;

        auto    buffer = Buffer( SIZE );

        auto    bufferPointers = BufferPointers( height );
        initBufferPointers(
            bufferPointers
            , buffer
            , ROW_STRIDE
            , SIZE
        );

        transform(
            _pngPtr
            , bitDepth
            , colorType
        );

        png_read_image(
            _pngPtr
            , bufferPointers.data()
        );

        png_read_end(
            _pngPtr
            , nullptr
        );

        _width = width;
        _height = height;
        _format = format;
        _dataType = dataType;
        _buffer = std::move( buffer );

        return true;
    }
}

namespace karameru {
    FgGLPicture * generateGLPictureFromPngImpl(
        const void *    _DATA
        , std::size_t   _size
    )
    {
        auto    pngReadStructs = PngReadStructs();

        auto &  pngPtr = pngReadStructs.pngPtr;
        pngPtr = png_create_read_struct(
            PNG_LIBPNG_VER_STRING
            , nullptr
            , nullptr
            , nullptr
        );
        if( pngPtr == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:PNG読み込み構造体の生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    pngReadStructsDestroyer = PngReadStructsDestroyer( &pngReadStructs );

        auto &  infoPtr = pngReadStructs.infoPtr;
        infoPtr = png_create_info_struct( pngPtr );
        if( infoPtr == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:PNG情報構造体の生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        if( setjmp( png_jmpbuf( pngPtr ) ) != 0 ) {
#ifdef  DEBUG
            std::printf( "E:PNG復号中にエラーが発生\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    width = fg::GLsizei();
        auto    height = fg::GLsizei();
        auto    format = fg::GLenum();
        auto    dataType = fg::GLenum();
        auto    buffer = Buffer();

        if( decompress(
            width
            , height
            , format
            , dataType
            , buffer
            , pngPtr
            , infoPtr
            , _DATA
            , _size
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:PNG復号化に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return fgGLPictureCreate(
            buffer.data()
            , buffer.size()
            , width
            , height
            , format
            , dataType
        );
    }
}
