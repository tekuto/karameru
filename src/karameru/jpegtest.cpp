﻿#include "fg/util/test.h"
#include "karameru/jpeg.h"
#include "fg/gl/picture.h"
#include "fg/gl/gl.h"
#include "karameru/test/picture.h"

#include <vector>
#include <array>
#include <memory>
#include <cstdio>

using FileData = std::vector< char >;
using Buffer = std::array< char, 1024 >;

struct CloseFile
{
    void operator()(
        std::FILE * _filePtr
    ) const
    {
        std::fclose( _filePtr );
    }
};

using FileCloser = std::unique_ptr<
    std::FILE
    , CloseFile
>;

void initFileData(
    FileData &      _fileData
    , const char *  _FILE_PATH
)
{
    auto    file = std::fopen(
        _FILE_PATH
        , "rb"
    );
    ASSERT_NE( nullptr, file );
    auto    fileCloser = FileCloser( file );

    auto    buffer = Buffer();

    auto    bufferPtr = buffer.data();

    const auto  BUFFER_IT = buffer.begin();
    const auto  BUFFER_SIZE = buffer.size();

    auto    fileData = FileData();
    while( std::feof( file ) == 0 ) {
        const auto  READ_SIZE = std::fread(
            bufferPtr
            , 1
            , BUFFER_SIZE
            , file
        );
        ASSERT_EQ( 0, std::ferror( file ) );

        fileData.insert(
            fileData.end()
            , BUFFER_IT
            , BUFFER_IT + READ_SIZE
        );
    }

    _fileData = std::move( fileData );
}

TEST(
    JpegTest
    , Generate
)
{
    auto    fileData = FileData();
    ASSERT_NO_FATAL_FAILURE(
        initFileData(
            fileData
            , "jpegtest/jpegtest.jpg"
        )
    );

    auto    pictureUnique = karameru::generateGLPictureFromJpeg(
        fileData.data()
        , fileData.size()
    );
    ASSERT_NE( nullptr, pictureUnique.get() );
    const auto &    PICTURE = *pictureUnique;

    ASSERT_EQ( 200 * 100 * 3, PICTURE->data.size() );
    ASSERT_EQ( 200, PICTURE->width );
    ASSERT_EQ( 100, PICTURE->height );
    ASSERT_EQ( fg::GL_RGB, PICTURE->format );
    ASSERT_EQ( fg::GL_UNSIGNED_BYTE, PICTURE->dataType );
}

TEST(
    JpegTest
    , Generate_grayScale
)
{
    auto    fileData = FileData();
    ASSERT_NO_FATAL_FAILURE(
        initFileData(
            fileData
            , "jpegtest/jpegtest_grayscale.jpg"
        )
    );

    auto    pictureUnique = karameru::generateGLPictureFromJpeg(
        fileData.data()
        , fileData.size()
    );
    ASSERT_NE( nullptr, pictureUnique.get() );
    const auto &    PICTURE = *pictureUnique;

    ASSERT_EQ( 200 * 100, PICTURE->data.size() );
    ASSERT_EQ( 200, PICTURE->width );
    ASSERT_EQ( 100, PICTURE->height );
    ASSERT_EQ( fg::GL_LUMINANCE, PICTURE->format );
    ASSERT_EQ( fg::GL_UNSIGNED_BYTE, PICTURE->dataType );
}

TEST(
    JpegTest
    , Generate_failed
)
{
    auto    fileData = FileData();
    ASSERT_NO_FATAL_FAILURE(
        initFileData(
            fileData
            , "jpegtest/jpegtest_failed.jpg"
        )
    );

    auto    pictureUnique = karameru::generateGLPictureFromJpeg(
        fileData.data()
        , fileData.size()
    );
    ASSERT_EQ( nullptr, pictureUnique.get() );
}

TEST(
    JpegTest
    , Generate_failedNotJpeg
)
{
    auto    fileData = FileData();
    ASSERT_NO_FATAL_FAILURE(
        initFileData(
            fileData
            , "jpegtest/jpegtest_failed.png"
        )
    );

    auto    pictureUnique = karameru::generateGLPictureFromJpeg(
        fileData.data()
        , fileData.size()
    );
    ASSERT_EQ( nullptr, pictureUnique.get() );
}
