﻿#include "fg/util/export.h"
#include "fg/gl/picture.h"
#include "karameru/test/picture.h"

#include <utility>

FgGLPicture * fgGLPictureCreate(
    const void *    _DATA_PTR
    , size_t        _size
    , FgGLsizei     _width
    , FgGLsizei     _height
    , FgGLenum      _format
    , FgGLenum      _dataType
)
{
    const auto  DATA_PTR = static_cast< const char * >( _DATA_PTR );

    auto    data = FgGLPicture::Data(
        DATA_PTR
        , DATA_PTR + _size
    );

    return new FgGLPicture{
        std::move( data ),
        _width,
        _height,
        _format,
        _dataType,
    };
}

void fgGLPictureDestroy(
    FgGLPicture *   _this
)
{
    delete _this;
}
