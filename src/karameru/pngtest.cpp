﻿#include "fg/util/test.h"
#include "karameru/png.h"
#include "fg/gl/picture.h"
#include "fg/gl/gl.h"
#include "karameru/test/picture.h"

#include <vector>
#include <array>
#include <memory>
#include <cstdio>

using FileData = std::vector< char >;
using Buffer = std::array< char, 1024 >;

struct CloseFile
{
    void operator()(
        std::FILE * _filePtr
    ) const
    {
        std::fclose( _filePtr );
    }
};

using FileCloser = std::unique_ptr<
    std::FILE
    , CloseFile
>;

void initFileData(
    FileData &      _fileData
    , const char *  _FILE_PATH
)
{
    auto    file = std::fopen(
        _FILE_PATH
        , "rb"
    );
    ASSERT_NE( nullptr, file );
    auto    fileCloser = FileCloser( file );

    auto    buffer = Buffer();

    auto    bufferPtr = buffer.data();

    const auto  BUFFER_IT = buffer.begin();
    const auto  BUFFER_SIZE = buffer.size();

    auto    fileData = FileData();
    while( std::feof( file ) == 0 ) {
        const auto  READ_SIZE = std::fread(
            bufferPtr
            , 1
            , BUFFER_SIZE
            , file
        );
        ASSERT_EQ( 0, std::ferror( file ) );

        fileData.insert(
            fileData.end()
            , BUFFER_IT
            , BUFFER_IT + READ_SIZE
        );
    }

    _fileData = std::move( fileData );
}

TEST(
    PngTest
    , Generate
)
{
    auto    fileData = FileData();
    ASSERT_NO_FATAL_FAILURE(
        initFileData(
            fileData
            , "pngtest/pngtest.png"
        )
    );

    auto    pictureUnique = karameru::generateGLPictureFromPng(
        fileData.data()
        , fileData.size()
    );
    ASSERT_NE( nullptr, pictureUnique.get() );
    const auto &    PICTURE = *pictureUnique;

    ASSERT_EQ( 200 * 100 * 3, PICTURE->data.size() );
    ASSERT_EQ( 200, PICTURE->width );
    ASSERT_EQ( 100, PICTURE->height );
    ASSERT_EQ( fg::GL_RGB, PICTURE->format );
    ASSERT_EQ( fg::GL_UNSIGNED_BYTE, PICTURE->dataType );
}

TEST(
    PngTest
    , Generate_withAlpha
)
{
    auto    fileData = FileData();
    ASSERT_NO_FATAL_FAILURE(
        initFileData(
            fileData
            , "pngtest/pngtest_withalpha.png"
        )
    );

    auto    pictureUnique = karameru::generateGLPictureFromPng(
        fileData.data()
        , fileData.size()
    );
    ASSERT_NE( nullptr, pictureUnique.get() );
    const auto &    PICTURE = *pictureUnique;

    ASSERT_EQ( 200 * 100 * 4, PICTURE->data.size() );
    ASSERT_EQ( 200, PICTURE->width );
    ASSERT_EQ( 100, PICTURE->height );
    ASSERT_EQ( fg::GL_RGBA, PICTURE->format );
    ASSERT_EQ( fg::GL_UNSIGNED_BYTE, PICTURE->dataType );
}

TEST(
    PngTest
    , Generate_16bit
)
{
    auto    fileData = FileData();
    ASSERT_NO_FATAL_FAILURE(
        initFileData(
            fileData
            , "pngtest/pngtest_16bit.png"
        )
    );

    auto    pictureUnique = karameru::generateGLPictureFromPng(
        fileData.data()
        , fileData.size()
    );
    ASSERT_NE( nullptr, pictureUnique.get() );
    const auto &    PICTURE = *pictureUnique;

    ASSERT_EQ( 200 * 100 * 2 * 3, PICTURE->data.size() );
    ASSERT_EQ( 200, PICTURE->width );
    ASSERT_EQ( 100, PICTURE->height );
    ASSERT_EQ( fg::GL_RGB, PICTURE->format );
    ASSERT_EQ( fg::GL_UNSIGNED_SHORT, PICTURE->dataType );
}

TEST(
    PngTest
    , Generate_grayScale
)
{
    auto    fileData = FileData();
    ASSERT_NO_FATAL_FAILURE(
        initFileData(
            fileData
            , "pngtest/pngtest_grayscale.png"
        )
    );

    auto    pictureUnique = karameru::generateGLPictureFromPng(
        fileData.data()
        , fileData.size()
    );
    ASSERT_NE( nullptr, pictureUnique.get() );
    const auto &    PICTURE = *pictureUnique;

    ASSERT_EQ( 200 * 100, PICTURE->data.size() );
    ASSERT_EQ( 200, PICTURE->width );
    ASSERT_EQ( 100, PICTURE->height );
    ASSERT_EQ( fg::GL_LUMINANCE, PICTURE->format );
    ASSERT_EQ( fg::GL_UNSIGNED_BYTE, PICTURE->dataType );
}

TEST(
    PngTest
    , Generate_grayScale1bit
)
{
    auto    fileData = FileData();
    ASSERT_NO_FATAL_FAILURE(
        initFileData(
            fileData
            , "pngtest/pngtest_grayscale1bit.png"
        )
    );

    auto    pictureUnique = karameru::generateGLPictureFromPng(
        fileData.data()
        , fileData.size()
    );
    ASSERT_NE( nullptr, pictureUnique.get() );
    const auto &    PICTURE = *pictureUnique;

    ASSERT_EQ( 200 * 100, PICTURE->data.size() );
    ASSERT_EQ( 200, PICTURE->width );
    ASSERT_EQ( 100, PICTURE->height );
    ASSERT_EQ( fg::GL_LUMINANCE, PICTURE->format );
    ASSERT_EQ( fg::GL_UNSIGNED_BYTE, PICTURE->dataType );
}

TEST(
    PngTest
    , Generate_grayScale4bit
)
{
    auto    fileData = FileData();
    ASSERT_NO_FATAL_FAILURE(
        initFileData(
            fileData
            , "pngtest/pngtest_grayscale4bit.png"
        )
    );

    auto    pictureUnique = karameru::generateGLPictureFromPng(
        fileData.data()
        , fileData.size()
    );
    ASSERT_NE( nullptr, pictureUnique.get() );
    const auto &    PICTURE = *pictureUnique;

    ASSERT_EQ( 200 * 100, PICTURE->data.size() );
    ASSERT_EQ( 200, PICTURE->width );
    ASSERT_EQ( 100, PICTURE->height );
    ASSERT_EQ( fg::GL_LUMINANCE, PICTURE->format );
    ASSERT_EQ( fg::GL_UNSIGNED_BYTE, PICTURE->dataType );
}

TEST(
    PngTest
    , Generate_grayScaleWithAlpha
)
{
    auto    fileData = FileData();
    ASSERT_NO_FATAL_FAILURE(
        initFileData(
            fileData
            , "pngtest/pngtest_grayscalewithalpha.png"
        )
    );

    auto    pictureUnique = karameru::generateGLPictureFromPng(
        fileData.data()
        , fileData.size()
    );
    ASSERT_NE( nullptr, pictureUnique.get() );
    const auto &    PICTURE = *pictureUnique;

    ASSERT_EQ( 200 * 100 * 2, PICTURE->data.size() );
    ASSERT_EQ( 200, PICTURE->width );
    ASSERT_EQ( 100, PICTURE->height );
    ASSERT_EQ( fg::GL_LUMINANCE_ALPHA, PICTURE->format );
    ASSERT_EQ( fg::GL_UNSIGNED_BYTE, PICTURE->dataType );
}

TEST(
    PngTest
    , Generate_palette
)
{
    auto    fileData = FileData();
    ASSERT_NO_FATAL_FAILURE(
        initFileData(
            fileData
            , "pngtest/pngtest_palette.png"
        )
    );

    auto    pictureUnique = karameru::generateGLPictureFromPng(
        fileData.data()
        , fileData.size()
    );
    ASSERT_NE( nullptr, pictureUnique.get() );
    const auto &    PICTURE = *pictureUnique;

    ASSERT_EQ( 200 * 100 * 3, PICTURE->data.size() );
    ASSERT_EQ( 200, PICTURE->width );
    ASSERT_EQ( 100, PICTURE->height );
    ASSERT_EQ( fg::GL_RGB, PICTURE->format );
    ASSERT_EQ( fg::GL_UNSIGNED_BYTE, PICTURE->dataType );
}

TEST(
    PngTest
    , Generate_paletteWithAlpha
)
{
    auto    fileData = FileData();
    ASSERT_NO_FATAL_FAILURE(
        initFileData(
            fileData
            , "pngtest/pngtest_palettewithalpha.png"
        )
    );

    auto    pictureUnique = karameru::generateGLPictureFromPng(
        fileData.data()
        , fileData.size()
    );
    ASSERT_NE( nullptr, pictureUnique.get() );
    const auto &    PICTURE = *pictureUnique;

    ASSERT_EQ( 200 * 100 * 4, PICTURE->data.size() );
    ASSERT_EQ( 200, PICTURE->width );
    ASSERT_EQ( 100, PICTURE->height );
    ASSERT_EQ( fg::GL_RGBA, PICTURE->format );
    ASSERT_EQ( fg::GL_UNSIGNED_BYTE, PICTURE->dataType );
}

TEST(
    PngTest
    , Generate_failed
)
{
    auto    fileData = FileData();
    ASSERT_NO_FATAL_FAILURE(
        initFileData(
            fileData
            , "pngtest/pngtest_failed.png"
        )
    );

    auto    pictureUnique = karameru::generateGLPictureFromPng(
        fileData.data()
        , fileData.size()
    );
    ASSERT_EQ( nullptr, pictureUnique.get() );
}
