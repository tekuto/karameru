﻿#ifndef KARAMERU_PNG_H
#define KARAMERU_PNG_H

#include "fg/def/gl/picture.h"
#include "fg/def/common/unique.h"
#include "fg/util/import.h"

#include <cstddef>

namespace karameru {
    FG_FUNCTION_PTR(
        FgGLPicture
        , generateGLPictureFromPngImpl(
            const void *
            , std::size_t
        )
    )

    inline fg::GLPicture::Unique generateGLPictureFromPng(
        const void *    _DATA
        , std::size_t   _size
    )
    {
        return generateGLPictureFromPngImpl(
            _DATA
            , _size
        );
    }
}

#endif  // KARAMERU_PNG_H
