﻿#ifndef KARAMERU_JPEG_H
#define KARAMERU_JPEG_H

#include "fg/def/gl/picture.h"
#include "fg/def/common/unique.h"
#include "fg/util/import.h"

#include <cstddef>

namespace karameru {
    FG_FUNCTION_PTR(
        FgGLPicture
        , generateGLPictureFromJpegImpl(
            const void *
            , std::size_t
        )
    )

    inline fg::GLPicture::Unique generateGLPictureFromJpeg(
        const void *    _DATA
        , std::size_t   _size
    )
    {
        return generateGLPictureFromJpegImpl(
            _DATA
            , _size
        );
    }
}

#endif  // KARAMERU_JPEG_H
