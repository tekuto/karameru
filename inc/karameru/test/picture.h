﻿#ifndef KARAMERU_TEST_PICTURE_H
#define KARAMERU_TEST_PICTURE_H

#include "fg/def/gl/picture.h"
#include "fg/def/gl/types.h"

#include <vector>

struct FgGLPicture
{
    using Data = std::vector< char >;

    Data    data;

    FgGLsizei   width;
    FgGLsizei   height;
    FgGLenum    format;
    FgGLenum    dataType;
};

#endif  // KARAMERU_TEST_PICTURE_H
